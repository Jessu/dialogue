class Report(var factory: String, var category: String, var info: String){
  
  def factoryDone(): Boolean = {
    if(factory.length > 0 ) return false
    else return true
  }
  def categoryDone(): Boolean = {
    if(category.length > 0) return false
    else return true
  }
  def reportDone(): Boolean = {
    if(info.length > 0) return false
    else return true
  }
  def reset(){
    factory = ""
    category = ""
    info = ""
  }
}

object cli {
  def main(args: Array[String]) {
    val r = new Report("","","")
    println("Hello!")
    while(r.reportDone){
      while (r.categoryDone){
        var x = scala.io.StdIn.readLine("What kind of problem do you have? \n")
        if(x=="help"){
          println("Possible problems:\n\nchild labor\ndangerous working enviroment\novertime\nabuse\n\nType one of the previous problems to continue\n")
        }
        else if(x=="reset")r.reset
        else if(x=="child labor"){
          while(r.categoryDone){
          var x1 = scala.io.StdIn.readLine("How many child are there working in the factory? \n")
          if(x1=="help")println("Type the amount of childs working in the factory.\n")
           else{
             r.category = x +" "+ x1
             println("Problem added to report\n")
           }
          }
        }
        else if(x=="dangerous working enviroment"){
          while(r.categoryDone){
          var x2 = scala.io.StdIn.readLine("What makes working dangerous?\n")
          if(x2=="help")println("Type in type of danger.\n\nExamples:\n\nNo mask\nOld structure\n")
           else{
             r.category = x +" "+ x2
             println("Problem added to report\n")
           }
          }
        }
        else if(x=="overtime"){
          while(r.categoryDone){
          var x3 = scala.io.StdIn.readLine("How many hours overtime you do? \n")
          if(x3=="help")println("Type the amount of hours you do.\n")
          else{
             r.category = x +" "+ x3
             println("Problem added to report\n")
           }
          }
        }
         else if(x=="abuse"){
          while(r.categoryDone){
          var x4 = scala.io.StdIn.readLine("What kind of abuse is happening?")
          if(x4=="help")println("Types of abuse:\n\nPhysical\nVerbal\n")
          else if(x4=="reset")r.reset
          else{
             r.category = x +" "+ x4
             println("Problem added to report\n")
           }
          }
        }
      }
    while (r.factoryDone){
        var y = scala.io.StdIn.readLine("Where do you work? \n")
            r.factory = y
             println("Your workplace has been added to the report\n")
        }
    var z = scala.io.StdIn.readLine("Is there something specific you want to add to report before sending it?\n\nPress enter if you don't have anything to add.\n\nYou can start over by typing reset \n")
    if(z=="reset"){
      r.reset
    }
    else{ r.info = z +" "
    println("Report has been sent.\n\nThank you for your report!")
    }
    }
  }
}